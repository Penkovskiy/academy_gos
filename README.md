Example [GitBook] website using GitLab with [Netlify](https://www.netlify.com/).

## Netlify Configuration

In order to build this site with Netlify, simply log in or register at 
https://app.netlify.com/, then select "New site from Git" from the top
right. Select GitLab, authenticate if needed, and then select this
project from the list. 

You will need to set the build command to `gitbook build` and set the publish
directory to `_book`. Netlify will handle the rest.

In the meantime, you can take advantage of all the great GitLab features
like merge requests, issue tracking, epics, and everything else GitLab has
to offer.

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] GitBook `npm install gitbook-cli -g`
1. Fetch GitBook's latest stable version `gitbook fetch latest`
1. Preview your project: `gitbook serve`
1. Add content
1. Generate the website: `gitbook build` (optional)
1. Push your changes to the master branch: `git push`

Read more at GitBook's [documentation].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

----


## Теплофізика

1. #### Складний теплообмін. термічний опір і коефіцієнт теплопередачі багатошарового огородження

 Складний теплообмін - коли всі три способи передачі теплоти відбуваються одночасно (теплопровідність, конвекція, теплове випромінювання).
У природі і техніці найчастіше зустрічаються наступні два варіанти складного теплообміну:

* тепловіддача – конвективний теплообмін між потоками рідини чи газу та поверхнею твердого тіла;

* теплопередача –  називається процес перенесення теплоти від більш нагрітого теплоносія до менш нагрітого теплоносія через роздільну стінку. 
Теплопередача являє собою складний теплообмін, який складається із ланцюжка окремих його видів. Від гарячого теплоносія до стінки перенесення теплоти здійснюється конвективним теплообміном. Усередині стінки теплота переноситься теплопровідністю. Від стінки до холодного теплоносія теплота переноситься конвективним теплообміном.

Термічний опір - здатність тіла перешкоджати поширенню теплового руху молекул. Термічний опір чисельно дорівнює температурному напору, необхідному для передачі одиничного теплового потоку. Термічний опір складної системи (наприклад, багатошарової теплової ізоляції) дорівнює сумі термічних опорів її частин.
```math
R = \frac{\Delta T}{Q}=\frac{\delta}{\lambda}

```
```math

R=\sum{R_i}
```
Коефіцієнт теплопередачі характеризує інтенсивність перенесення теплоти і являє собою тепловий потік для одиничного температурного напору, Вт/(м2·К).
```math

k = \frac{1}{\frac{1}{a_з} + \sum\frac{\delta}{\lambda} +\frac{1}{a_в} } = \frac{1}{R_з + R_{ст} + R_в }
```
[link](https://web.posibnyky.vntu.edu.ua/fbteg/chepurnij_teplomasoob/7.htm)

2. #### Розрахунок товщини утеплювача за мінімально допустимим та необхідним опорами теплопередачі

[link](https://studfiles.net/preview/5532466/)

За ДБН "Теплова iзоляцiя будiвель" за зоною будiвництва визначаємо мінімально допустиме значення опору теплопередачі огороджувальної конструкції $`R_{q.min}`$
Визначаємо товщину утеплюючого шару огороджувальної конструкції
$`\delta_{ут} = [R_{q.min} - (\frac{1}{a_в} + \sum^{n-1}\frac{\delta_i}{\lambda_i} + \frac{1}{a_з})]\cdot \lambda_{ут} `$

де  $`a_в,  a_з`$ -  коефіцієнти тепловіддачі внутрішньої і зовнішньої поверхонь огороджувальної конструкції
	
$`\delta_i`$ - товщина і-го шару конструкції, м;

$`\lambda_i`$ - теплопровідність матеріалу 

Приводимо товщину утеплюючого шару до конструктивних розмірів,  Визначаємо приведений опір теплопередачі 


3. #### Розрахунок теплотривкості огороджень у літній період

Теплостійкість - здатність теплоізоляційних матеріалів витримувати без зміни структури і руйнування періодичні коливання температури

 


4. #### Розрахунок повітропроникності конструкцій що огороджують
5. #### Розрахунок вологісного режиму огороджень при наявності зони конденсації
6. #### Розрахунок розподілу температур в шарах огородження аналітичним та графічним методами
7. #### Вологість огородження: причини наслідки. параметри, що визначають вологісний режим
8. #### Види теплообміну основні поняття визначеннч питомого теплового потоку коефіціенти які характеризують кожен вид теплообміну

---



Forked from @virtuacreative

[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
